package ch.hevs.algorithmMethods;

import java.util.HashMap;

/**
 * This final class will regroup all classifier methods in static
 * @author Lucien
 *
 */
public class ClassifierMethods {		

	private ClassifierMethods(){
		
	}
	
	public static final HashMap<String, String>abstractClassifierParameters;
	static
	{
		abstractClassifierParameters = new HashMap<String, String>();
		abstractClassifierParameters.put("-do-not-check-capabilities" , "Do Not Check Capabilities");
		abstractClassifierParameters.put("-num-decimal-places" , "Num Decimal Places");
		abstractClassifierParameters.put("-batch-size" , "Batch Size");
	}
	
	public static final HashMap<String, String>abstractClassifierDefaultParameters;
	static
	{
		abstractClassifierDefaultParameters = new HashMap<String, String>();
		abstractClassifierDefaultParameters.put("-do-not-check-capabilities" , "Do Not Check Capabilities");
		abstractClassifierDefaultParameters.put("-num-decimal-places" , "Num Decimal Places");
		abstractClassifierDefaultParameters.put("-batch-size" , "Batch Size");
	}

	public static final HashMap<String, String>SupportVectorMachineModelParameters;
	static
	{
		SupportVectorMachineModelParameters = new HashMap<String, String>();
		SupportVectorMachineModelParameters.put("TEST", "TEST");
	}
	
	public static final HashMap<String, String>SupportVectorMachineModelDefaultParameters;
	static
	{
		SupportVectorMachineModelDefaultParameters = new HashMap<String, String>();
		SupportVectorMachineModelDefaultParameters.put("TEST", "TEST");
	}
	
	public static final HashMap<String, String>RandomForestParameters;
	static
	{
		
		RandomForestParameters = new HashMap<String, String>();
		RandomForestParameters.put("-P", "Bag Size");
//		RandomForestParameters.put("-O", "Out of the bag error");
//		RandomForestParameters.put("-store-out-of-bag-predictions", "store out of bag predictions in internal evaluation object");
//		RandomForestParameters.put("-output-out-of-bag-complexity-statistics", "output complexity-based statistics when out-of-bag evaluation is performed");
//		RandomForestParameters.put("-print", "Print the individual classifiers in the output");
//		RandomForestParameters.put("-attribute-importance", "Compute and output attribute importance");
		RandomForestParameters.put("-I", "Number of iterations");
		RandomForestParameters.put("-num-slots", "Number of execution slots");
		RandomForestParameters.put("-K", "Number of attribute to randomly investigate");
		RandomForestParameters.put("-M", "minimum number of instances per leaf");
//		RandomForestParameters.put("-V", "minimum numeric class variance proportion of train variance for split ");
		RandomForestParameters.put("-S", "Seed for random number generator");
		RandomForestParameters.put("-depth", "The maximum depth of the tree, 0 for unlimited");
		RandomForestParameters.put("-N", "Number of folds for backfitting");
//		RandomForestParameters.put("-U", "Allow unclassified instances");
//		RandomForestParameters.put("-B", "Break ties randomly when several attributes look equally good");
//		RandomForestParameters.put("-output-debug-info", "run classifier in debug mod");
//		RandomForestParameters.put("-do-not-check-capabilities", "check classifier capabilities before classifier is built");
		RandomForestParameters.put("-num-decimal-places", "number of decimal places for the output of numbers in the model");
		RandomForestParameters.put("-batch-size", "desired batch size for batch prediction  ");
	}
	public static final HashMap<String, String>RandomForestDefaultParameters;
	static
	{
		RandomForestDefaultParameters = new HashMap<String, String>();
		RandomForestDefaultParameters.put("-P", "100");
//		RandomForestDefaultParameters.put("-O", "False");
//		RandomForestDefaultParameters.put("-store-out-of-bag-predictions", "False");
//		RandomForestDefaultParameters.put("-output-out-of-bag-complexity-statistics", "False");
//		RandomForestDefaultParameters.put("-print", "False");
//		RandomForestDefaultParameters.put("-attribute-importance", "False"); // !
		RandomForestDefaultParameters.put("-I", "100");
		RandomForestDefaultParameters.put("-num-slots", "1");
		RandomForestDefaultParameters.put("-K", "0");
		RandomForestDefaultParameters.put("-M", "1");
//		RandomForestDefaultParameters.put("-V", "1e-3");
		RandomForestDefaultParameters.put("-S", "1");
		RandomForestDefaultParameters.put("-depth", "0");
		RandomForestDefaultParameters.put("-N", "0");
//		RandomForestDefaultParameters.put("-U", "False"); //!
//		RandomForestDefaultParameters.put("-B", "False");
//		RandomForestDefaultParameters.put("-output-debug-info", "False");
//		RandomForestDefaultParameters.put("-do-not-check-capabilities", "False");
		RandomForestDefaultParameters.put("-num-decimal-places", "2");
		RandomForestDefaultParameters.put("-batch-size", "100");
	}
	
	public static final HashMap<String, String>MultilayerPerceptronDefaultParameters;
	static
	{
		MultilayerPerceptronDefaultParameters = new HashMap<String, String>();
		MultilayerPerceptronDefaultParameters.put("-L", "0.3");
		MultilayerPerceptronDefaultParameters.put("-M", "0.2");
		MultilayerPerceptronDefaultParameters.put("-N", "2");
		MultilayerPerceptronDefaultParameters.put("-V", "0");
		MultilayerPerceptronDefaultParameters.put("-S", "0");
		MultilayerPerceptronDefaultParameters.put("-E", "20");
//		MultilayerPerceptronDefaultParameters.put("-G", "False");
//		MultilayerPerceptronDefaultParameters.put("-A", "False");
//		MultilayerPerceptronDefaultParameters.put("-B", "True");
		MultilayerPerceptronDefaultParameters.put("-H", "a");
//		MultilayerPerceptronDefaultParameters.put("-C", "True");
//		MultilayerPerceptronDefaultParameters.put("-I", "True");
//		MultilayerPerceptronDefaultParameters.put("-R", "False");
//		MultilayerPerceptronDefaultParameters.put("-D", "False");

	}
	
	public static final HashMap<String, String>MultilayerPerceptronParameters;
	static
	{
		MultilayerPerceptronParameters = new HashMap<String, String>();
		MultilayerPerceptronParameters.put("-L", "Learning Rate for the backpropagation algorithm (between 0 - 1)");
		MultilayerPerceptronParameters.put("-M", "Momentum Rate for the backpropagation algorithm(between 0 - 1)");
		MultilayerPerceptronParameters.put("-N", "Number of epochs to train through");
		MultilayerPerceptronParameters.put("-V", "Percentage size of validation set to use to terminate training (between 0 - 100)");
		MultilayerPerceptronParameters.put("-S", "The value used to seed the random number generator (value > 0)");
		MultilayerPerceptronParameters.put("-E", "The consequetive number of errors allowed for validation testing before the network terminates (value > 0)");
//		MultilayerPerceptronParameters.put("-G", "GUI will be opened");
//		MultilayerPerceptronParameters.put("-A", "Autocreation of the network connections will NOT be done");
//		MultilayerPerceptronParameters.put("-B", "A NominalToBinary filter will NOT automatically be used");
		MultilayerPerceptronParameters.put("-H", "The hidden layers to be created for the network");
//		MultilayerPerceptronParameters.put("-C", "Normalizing a numeric class will NOT be done");
//		MultilayerPerceptronParameters.put("-I", "Normalizing the attributes will NOT be done");
//		MultilayerPerceptronParameters.put("-R", "Reseting the network will NOT be allowed");
//		MultilayerPerceptronParameters.put("-D", "Learning rate decay will occur");
	}

	public static final HashMap<String, String>logisticParameters;
	static
	{
		logisticParameters = new HashMap<String, String>();
		logisticParameters.put("-R", "ridge");
		logisticParameters.put("-M", "maximum number of iterations");
	}

	public static final HashMap<String, String>logisticDefaultParameters;
	static
	{
		logisticDefaultParameters = new HashMap<String, String>();
		logisticDefaultParameters.put("-R", "1.0E-8");
		logisticDefaultParameters.put("-M", "-1");
	}
	
	public static final HashMap<String, HashMap<String, String>> choosenAlgoParameters;
	static
	{
		choosenAlgoParameters = new HashMap<String, HashMap<String, String>>();
//		choosenAlgoParameters.put("SupportVectorMachineModel", SupportVectorMachineModelParameters);
//		choosenAlgoParameters.put("RandomForest", RandomForestParameters);
		choosenAlgoParameters.put("MultilayerPerceptron", MultilayerPerceptronParameters);
		choosenAlgoParameters.put("Logistic", logisticParameters);
	}
	public static final HashMap<HashMap<String, String>, HashMap<String, String>> choosenAlgoDefaultParameters;
	static
	{
		choosenAlgoDefaultParameters = new HashMap<HashMap<String, String>, HashMap<String, String>>();
//		choosenAlgoDefaultParameters.put(SupportVectorMachineModelParameters, SupportVectorMachineModelDefaultParameters);
//		choosenAlgoDefaultParameters.put(RandomForestParameters, RandomForestDefaultParameters);
		choosenAlgoDefaultParameters.put(MultilayerPerceptronParameters, MultilayerPerceptronDefaultParameters);
		choosenAlgoDefaultParameters.put(logisticParameters, logisticDefaultParameters);
	}
}