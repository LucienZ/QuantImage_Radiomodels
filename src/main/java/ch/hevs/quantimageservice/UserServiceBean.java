package ch.hevs.quantimageservice;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ch.hevs.businessobject.User;

@Stateless
public class UserServiceBean implements UserService {

	@PersistenceContext(unitName = "Quantimage_Radiomodels")
	private EntityManager em;

	
	@Override
	public User registerUser(String firstName, String lastName, String eMail, String password) {
		User user = getUser(eMail);

		if(user!=null){
			System.out.println("A user already exists with the email: "+user.geteMail()+" !");
			return null;
		}

		user = new User(firstName, lastName, eMail, password);
		em.persist(user);

		return user;
	}

	@Override
	public User getUser(String eMail) {
		User userResult;
		try{
			Query query = em.createQuery("SELECT u FROM User u WHERE u.eMail LIKE :eMail")
					.setParameter("eMail", eMail);
			userResult = (User) query.getSingleResult();
		}catch (NoResultException e) {
			userResult = null;
		}
		return userResult;
	}

	@Override
	public User getUser(String eMail, String password) {
		User userResult;
		try{
			Query query = em.createQuery("SELECT u FROM User u WHERE u.eMail LIKE :eMail AND u.password LIKE :password")
					.setParameter("eMail", eMail).setParameter("password", password);
			userResult = (User) query.getSingleResult();
		}catch (NoResultException e) {
			userResult = null;
		}

		return userResult;
	}
}
