package ch.hevs.quantimageservice;

import java.io.FileNotFoundException;
import java.util.HashMap;

import javax.ejb.Local;

import ch.hevs.businessobject.User;

/**
 * This interface will implement the stateful service layer for the instance management
 * @author Lucien
 *
 */
@Local
public interface InstanceServiceStateful {

	/**
	 * This method will initiate the quantimageInstance get by the instance name
	 * @param instanceName
	 */
	public void initiateQuantimageInstance(String instanceName);

	/**
	 * This class will create a new quantimage instance
	 * @param instanceName
	 * @param csvFile
	 * @param owner
	 * @param isNormalized
	 * @param featuresName
	 */
	public void createQuantimageInstance(String instanceName, String csvFilePath, String csvRowSeparator, User owner, boolean isNormalized, String[] featuresName);		
	/**
	 * This method will preprocess the data of the instances (normalize it or not)
	 * @param normalize
	 * @param className
	 * @param selectedHeaders
	 * @param selectedHeaders 
	 */
	public void submitPreprocessing(boolean normalize, String className, String selectedHeaders);

	/**
	 * This method will save the csv on the computer
	 * @param instanceName
	 * @param csvFile
	 * @return the path to the csv
	 * @throws FileNotFoundException
	 */
	public String saveCSV(String instanceName, String csvFile) throws FileNotFoundException;

	/**
	 * This method will create a new quantimage model
	 * @param modelName
	 * @param usedAlgorithm
	 * @param evaluationResult
	 * @param numberOfFolds
	 * @param evaluationType
	 * @param isClassifier
	 * @param ignoredFeatures
	 * @throws FileNotFoundException
	 */
	public void createQuantimageModel(String modelName, String usedAlgorithm, String evaluationResult, int numberOfFolds,
			String evaluationType, boolean isClassifier, String ignoredFeatures) throws FileNotFoundException;

	/**
	 * This method will serialize the model
	 * @param modelName
	 * @throws FileNotFoundException
	 */
	public void serializeModel(String modelName, String modelFilePath) throws FileNotFoundException;

	/**
	 * This method will build the classifier and the validation
	 * @param choosenAlgorithmParametersHashMap
	 * @param choosenAlgorithm
	 * @param numberOfFolds
	 * @return the validation result
	 */
	public String AlgorithmMethod(HashMap<String, String> choosenAlgorithmParametersHashMap, String choosenAlgorithm, int numberOfFolds);

	/**
	 * This method will create a new instance
	 * @param csvFilePath
	 * @param csvRowSeparator
	 */
	public void createInstance(String csvFilePath, String csvRowSeparator);

}
