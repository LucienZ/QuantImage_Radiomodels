package ch.hevs.quantimageservice;

import javax.ejb.Local;

import ch.hevs.businessobject.User;

/**
 * This interface is the service layer for the user
 * @author Lucien
 *
 */
@Local
public interface UserService {
	/**
	 * This method will register a new user
	 * @param firstName
	 * @param lastName
	 * @param eMail
	 * @param password
	 * @return user
	 */
	public User registerUser(String firstName, String lastName, String eMail, String password);
		
	/**
	 * this method will return a single user with the mail
	 * @param eMail
	 * @return the user
	 */
	public User getUser(String eMail);
	
	/**
	 * This method will return a single user with the mail and password
	 * @param eMail
	 * @param password
	 * @return the user
	 */
	public User getUser(String eMail, String password);	
}
