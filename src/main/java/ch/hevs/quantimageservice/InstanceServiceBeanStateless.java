package ch.hevs.quantimageservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ch.hevs.businessobject.QuantimageClassifier;
import ch.hevs.businessobject.QuantimageInstance;
import ch.hevs.businessobject.QuantimageModel;
import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.Remove;

/**
 * This class will implement the service bean for instance in stateless
 * @author Lucien
 *
 */
@Stateless
public class InstanceServiceBeanStateless implements InstanceServiceStateless {

	@PersistenceContext(unitName = "Quantimage_Radiomodels")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getInstancesByUser(Long userId) {
		List<String> instancesResult;
		try{
			Query query = em.createQuery("SELECT qi.name FROM QuantimageInstance qi WHERE qi.owner.id = :userId")
					.setParameter("userId", userId);
			instancesResult = query.getResultList();
		}catch (NoResultException e) {
			instancesResult = null;
		}
		return instancesResult;
	}

	@Override
	public QuantimageInstance getInstancesByName(String instanceName) {
		QuantimageInstance instanceResult;
		try{
			Query query = em.createQuery("SELECT qi FROM QuantimageInstance qi WHERE qi.name = :name")
					.setParameter("name", instanceName);
			instanceResult = (QuantimageInstance) query.getSingleResult();
		}catch (NoResultException e) {
			instanceResult = null;
		}
		return instanceResult;
	}

	@Override
	public String getCSVFile(String csvFilePath) {
		String csvFile ="";
		String line;
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(csvFilePath));
			while ((line = br.readLine()) != null) {
				csvFile += line+"\n";
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		return csvFile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getModelsByUser(Long userId) {
		List<String> modelsResult;
		try{
			Query query = em.createQuery("SELECT qm.name FROM QuantimageModel qm WHERE qm.instance.owner.id = :userId")
					.setParameter("userId", userId);
			modelsResult = query.getResultList();
		}catch (NoResultException e) {
			modelsResult = null;
		}
		return modelsResult;
	}

	@Override
	public QuantimageModel getModelByName(String modelName) {
		QuantimageModel modelResult;
		try{
			Query query = em.createQuery("SELECT qm FROM QuantimageModel qm WHERE qm.name = :name")
					.setParameter("name", modelName);
			modelResult = (QuantimageModel) query.getSingleResult();
		}catch (NoResultException e) {
			modelResult = null;
		}
		return modelResult;
	}

	@Override
	public String testModel(String modelName, String csvFile, String csvRowSeparator) {
		QuantimageModel quantimageModel = getModelByName(modelName);
		Instances testedInstance = createInstancesFromNewFile(csvFile, csvRowSeparator);
		String choosenAlgorithm = ((QuantimageClassifier)quantimageModel.getAlgorithm()).getClassifierName();
		String results = "empty";		
		Instances baseInstances = reCreateInstance(quantimageModel.getInstance().getCsvFilePath(), quantimageModel.getInstance().getCsvRowSeparator());

		//Check if instance is valid
		if(!testedInstance.checkInstance(baseInstances.instance(1))){
			return "NOTCOMPATIBLE";
		}

		//SetUp tested instance class
		testedInstance.setClass(testedInstance.attribute(quantimageModel.getClassAtributeName()));
		try {
			//Deserialize classifier
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(quantimageModel.getModelPath()));
			Classifier classifier = (Classifier) ois.readObject();
			ois.close();
			//Set the class to nominal
			NumericToNominal convert= new NumericToNominal();
			String[] converterOptions= new String[2];
			converterOptions[0]="-R";
			converterOptions[1]=testedInstance.classIndex()+1+"";  //range of variables to make numeric
			convert.setOptions(converterOptions);
			convert.setInputFormat(testedInstance);
			testedInstance=Filter.useFilter(testedInstance, convert);
			
			//Remove the unconsidered attributes
			String[] selectedHeadersArray = quantimageModel.getIgnoredFeatures().split(",");
			if(!quantimageModel.getIgnoredFeatures().equals("")){
				String indexesToRemove ="";
				for(int i=0; i<selectedHeadersArray.length-1; i++){
					if(!selectedHeadersArray[i].equals(testedInstance.classAttribute().name())){
						indexesToRemove+=testedInstance.attribute(selectedHeadersArray[i]).index()+1+"-";
					}
				}
				if(!selectedHeadersArray[selectedHeadersArray.length-1].equals(testedInstance.classAttribute().name())){
					indexesToRemove+=testedInstance.attribute(selectedHeadersArray[selectedHeadersArray.length-1]).index()+1;
				}
				String[] removeAttribute = {"-R", indexesToRemove};      
				try {
					Remove remove = new Remove();                         	
					remove.setOptions(removeAttribute);                     
					remove.setInputFormat(testedInstance);     
					testedInstance = Filter.useFilter(testedInstance, remove);
				} catch (Exception e) {
					System.err.println(e.getMessage());
					System.err.println("Can't suppress the attributes !");
				}
			}
			double[] resultsDouble = classifier.distributionForInstance(testedInstance.firstInstance());
			results = "";
			for (int i = 0; i < resultsDouble.length; i++) {
				testedInstance.classAttribute();
				results += "Chances of finding "+i+": "+(resultsDouble[i]*100)+"%. <br />";
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Can't test the "+choosenAlgorithm+" classifier !");
		}

		return results;
	}


	@Override
	public Instances createInstancesFromNewFile(String csvFile, String csvRowSeparator) {
		Instances instance = null;
		try {
			File temp = new File("/tmp/tempCSVQuantimageRadiomodels.csv");
			PrintWriter pw = new PrintWriter(temp);
			pw.write(csvFile);
			pw.close();
			CSVLoader csvLoader = new CSVLoader();
			csvLoader.setFile(temp);
			csvLoader.setFieldSeparator(csvRowSeparator);
			instance = csvLoader.getDataSet();
			temp.deleteOnExit();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Can't create the instance !");
		}
		return instance;
	}

	@Override
	public Instances reCreateInstance(String csvFilePath, String csvRowSeparator) {
		Instances instance = null;
		try {
			CSVLoader csvLoader = new CSVLoader();
			csvLoader.setFile(new File(csvFilePath));
			csvLoader.setFieldSeparator(csvRowSeparator);
			instance = csvLoader.getDataSet();
			return instance;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Can't create the instance !");
		}
		return null;
	}
}
