package ch.hevs.quantimageservice;

import java.util.List;

import javax.ejb.Local;

import ch.hevs.businessobject.QuantimageInstance;
import ch.hevs.businessobject.QuantimageModel;
import weka.core.Instances;

/**
 * This interface is the instance for the service in stateless
 * @author Lucien
 *
 */
@Local
public interface InstanceServiceStateless {
	
	/**
	 * This method will return the list of instance's nameof a given user
	 * @param userId
	 * @return the list of instance's name
	 */
	public List<String> getInstancesByUser(Long userId);
	
	/**
	 * This method will get an instance by its name
	 * @param instanceName
	 * @returnthe instance
	 */
	public QuantimageInstance getInstancesByName(String instanceName);
	
	/**
	 * This method will return the csv file in a given path
	 * @param csvFilePath
	 * @return the csvFile
	 */
	public String getCSVFile(String csvFilePath);

	/**
	 * This method will return the list of models name by a user
	 * @param id
	 * @return the list of models name
	 */
	public List<String> getModelsByUser(Long id);

	/**
	 * This method will return a model by its name
	 * @param modelName
	 * @return the quantimageModel
	 */
	public QuantimageModel getModelByName(String modelName);
	
	/**
	 * This method will execute a model on a csvFile
	 * @param modelName
	 * @param csvFile
	 * @param csvRowSeparator
	 * @return the result
	 */
	public String testModel(String modelName, String csvFile, String csvRowSeparator);

	/**
	 * This method will create and retun an instance from a csvFilePath and its row separator
	 * @param csvFilePath
	 * @param csvRowSeparator
	 * @return the created instances
	 */
	public Instances createInstancesFromNewFile(String csvFilePath, String csvRowSeparator);

	/**
	 * This method will create An instances
	 * @param csvFilePath
	 * @param csvRowSeparator
	 * @return the selected instances
	 */
	public Instances reCreateInstance(String csvFilePath, String csvRowSeparator);

	
}
