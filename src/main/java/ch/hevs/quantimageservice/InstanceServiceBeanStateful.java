package ch.hevs.quantimageservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import ch.hevs.algorithmMethods.ClassifierMethods;
import ch.hevs.businessobject.QuantimageAlgorithm;
import ch.hevs.businessobject.QuantimageClassifier;
import ch.hevs.businessobject.QuantimageClusterer;
import ch.hevs.businessobject.QuantimageEvaluation;
import ch.hevs.businessobject.QuantimageInstance;
import ch.hevs.businessobject.QuantimageModel;
import ch.hevs.businessobject.User;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.NumericToNominal;
import weka.filters.unsupervised.attribute.Remove;

/**
 * This class is the implementation of the instance service bean
 * @author Lucien
 *
 */
@Stateful
public class InstanceServiceBeanStateful implements InstanceServiceStateful {
	private Instances instance;
	private Classifier classifier;
	private QuantimageInstance quantInstance;
	private String options;

	@PersistenceContext(name = "Quantimage_Radiomodels", type=PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Override
	public void createQuantimageInstance(String instanceName, String csvFilePath, String csvRowSeparator, User owner, boolean isNormalized, String[]featuresName) {
		String features = "";
		for (int i = 0; i < featuresName.length; i++) {
			features += featuresName[i] + " ";
		}
		quantInstance = new QuantimageInstance(instanceName, csvFilePath, csvRowSeparator, owner, isNormalized, features);
		em.persist(quantInstance);
	}

	@Override
	public void initiateQuantimageInstance(String instanceName){
		try{
			Query query = em.createQuery("SELECT qi FROM QuantimageInstance qi WHERE qi.name = :name")
					.setParameter("name", instanceName);
			quantInstance = (QuantimageInstance) query.getSingleResult();
		}catch (NoResultException e) {
			quantInstance = null;
		}
	}

	@Override
	public String saveCSV(String instanceName, String csvFile) throws FileNotFoundException{
		Path folderPath = Paths.get("/home/lucien/data/CSV");
		String csvFilePath = folderPath.toString() + "/"+instanceName + ".csv";
		PrintWriter pw = new PrintWriter(new File(csvFilePath));

		pw.write(csvFile);
		pw.close();

		return csvFilePath;
	}


	@Override
	public void createInstance(String csvFilePath, String csvRowSeparator) {
		try {
			CSVLoader csvLoader = new CSVLoader();
			csvLoader.setFile(new File(csvFilePath));
			csvLoader.setFieldSeparator(csvRowSeparator);
			instance = csvLoader.getDataSet();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Can't create the instance !");
		}
	}

	@Override
	public void submitPreprocessing(boolean normalize, String className, String selectedHeaders){
		//Delete extra headers
		String[] selectedHeadersArray = selectedHeaders.split(",");
		if(!selectedHeaders.equals("")){
			String indexesToRemove ="";
			for(int i=0; i<selectedHeadersArray.length-1; i++){
				if(!selectedHeadersArray[i].equals(className)){
					indexesToRemove+=instance.attribute(selectedHeadersArray[i]).index()+1+"-";
				}
			}
			if(!selectedHeadersArray[selectedHeadersArray.length-1].equals(className)){
				indexesToRemove+=instance.attribute(selectedHeadersArray[selectedHeadersArray.length-1]).index()+1;
			}
			String[] removeAttribute = {"-R", indexesToRemove};      
			try {
				Remove remove = new Remove();                         	
				remove.setOptions(removeAttribute);                     
				remove.setInputFormat(instance);     
				instance = Filter.useFilter(instance, remove);
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.err.println("Can't supress the attributes !");
			}
		}
		//Normalize the instance
		quantInstance.setNormalize(normalize);
		if(quantInstance.isNormalize()){
			try {
				Normalize filterNorm = new Normalize();
				filterNorm.setInputFormat(instance);
				instance = Filter.useFilter(instance, filterNorm);
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.err.println("Normalized failed !");
			}
		}
		//Set up the class Instance
		instance.setClass(instance.attribute(className));

	}

	@Override
	public void createQuantimageModel(String modelName, String usedAlgorithm, String evaluationResult, int numberOfFolds, String evaluationType, boolean isClassifier, String ignoredFeatures) throws FileNotFoundException {
		Path folderPath = Paths.get("/home/lucien/data/Models");
		String modelFilePath = folderPath.toString() + "/" + modelName + ".model";
		QuantimageModel quantModel = new QuantimageModel(modelName, modelFilePath, instance.classAttribute().name(), ignoredFeatures);
		em.persist(quantModel);
		QuantimageAlgorithm algorithm;
		if(isClassifier){
			algorithm = new QuantimageClassifier(usedAlgorithm, options);
		}
		else{
			algorithm = new QuantimageClusterer(usedAlgorithm, options);
		}
		quantInstance.addModel(quantModel);
		quantModel.setAlgorithm(algorithm);
		QuantimageEvaluation evaluation = new QuantimageEvaluation(numberOfFolds, evaluationResult, evaluationType);
		quantModel.setEvaluation(evaluation);
		serializeModel(modelName, modelFilePath);
	}

	@Override
	public String AlgorithmMethod(HashMap<String, String> choosenAlgorithmParametersHashMap, String choosenAlgorithm, int numberOfFolds){
		String evalResult = "empty";

		String [] classifierOptions = new String [choosenAlgorithmParametersHashMap.size()*2];

		Iterator<Entry<String, String>> it = choosenAlgorithmParametersHashMap.entrySet().iterator();
		int i = 0;

		while (it.hasNext()) {
			Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
			classifierOptions[i] = getKeyByValue(ClassifierMethods.choosenAlgoParameters.get(choosenAlgorithm), pair.getKey());
			i++;
			classifierOptions[i] = pair.getValue();
			i++;
		}

		options = "";
		for (int j = 0; j < classifierOptions.length-1; j++) {
			options += classifierOptions[j] + " ";
		}
		options += classifierOptions[classifierOptions.length-1];

		try {
			NumericToNominal convert= new NumericToNominal();
			String[] converterOptions= new String[2];
			converterOptions[0]="-R";
			converterOptions[1]=instance.classIndex()+1+"";  //range of variables to make numeric
			convert.setOptions(converterOptions);
			convert.setInputFormat(instance);
			instance=Filter.useFilter(instance, convert);
			switch(choosenAlgorithm){
			case "SupportVectorMachineModel":

				break;

			case "RandomForest":
				classifier=new RandomForest();
				((RandomForest) classifier).setOptions(classifierOptions);
				break;

			case "MultilayerPerceptron":
				classifier=new MultilayerPerceptron();
				((MultilayerPerceptron) classifier).setOptions(classifierOptions);
				break;

			case "Logistic":
				classifier=new Logistic();
				((Logistic) classifier).setOptions(classifierOptions);
				break;

			default:
				System.err.println("No algorithm selected !");
				break;
			}
			Evaluation eval = new Evaluation(instance);
			eval.crossValidateModel(classifier, instance, numberOfFolds, new Random(1), new Object[0]);
			evalResult = (eval.toSummaryString("\nResults\n======", false));
			evalResult = evalResult.replaceAll("\\n", "<br />");

		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println("Can't create the "+choosenAlgorithm+" classifier !");
		}

		return evalResult;
	}

	@Override
	public void serializeModel(String modelName, String modelFilePath) throws FileNotFoundException{
		try {
			classifier.buildClassifier(instance);
		} catch (Exception e1) {
			System.err.println(e1.getMessage());
		}

		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(modelFilePath));
			oos.writeObject(classifier);
			oos.flush();
			oos.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.err.println("serialization failed !");
		}
	}

	private String getKeyByValue(HashMap<String, String> hashMap, String value){
		for (Entry<String, String> entry : hashMap.entrySet()) {
			if (value.equals(entry.getValue())) {
				return entry.getKey();
			}
		}
		return null;
	}
}
