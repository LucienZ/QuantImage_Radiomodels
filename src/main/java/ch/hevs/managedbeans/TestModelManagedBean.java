package ch.hevs.managedbeans;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Part;

import ch.hevs.quantimageservice.InstanceServiceStateless;

/**
 * This bean is used to upload a CSV
 * @author Lucien
 *
 */
public class TestModelManagedBean
{
//	private InstanceServiceStateful instanceFul;
	private InstanceServiceStateless instanceLess;
	private UserManagedBean userManagedBean;

	private String modelName;
	private List<String>listModelsName;

	private Part csvFilePart;
	private String csvFile;
	private String csvFileOneLine;
	private String csvRowSeparator;
	private String result;
	
	@PostConstruct
	public void initialize() {
		// use JNDI to inject reference to Quantimage EJB
		InitialContext ctx;
		try {
			ctx = new InitialContext();
//			instanceFul = (InstanceServiceStateful) ctx.lookup("java:global/Quantimage_Radiomodels-0.0.1-SNAPSHOT/InstanceServiceBeanStateful!ch.hevs.quantimageservice.InstanceServiceStateful");
			instanceLess = (InstanceServiceStateless) ctx.lookup("java:global/QuantImage_Radiomodels/InstanceServiceBeanStateless");
		} catch (NamingException e) {
			System.out.println("Can't initialize Context !");
			System.out.println(e.getMessage());
		}
	}

	/**
	 * This method will set up the list of models name from the user
	 * @return
	 */
	public String setUpListOfModelsName(){
		listModelsName = instanceLess.getModelsByUser(userManagedBean.getUser().getId());
		
		return "testModel.xhtml";
	}
	
	/**
	 * This method will test if the model is compatible and display the result if yes
	 * @return
	 */
	@SuppressWarnings("resource")
	public String IsTheModelCompatible(){
		try {
			csvFile = new Scanner(csvFilePart.getInputStream())
					.useDelimiter("\\A").next();
			csvFileOneLine = csvFile.replaceAll("(\\r|\\n|\\r\\n)", "#");
			csvFileOneLine = csvFileOneLine.replaceAll("##", "#");
			result = instanceLess.testModel(modelName, csvFile, csvRowSeparator);
			if(result.equals("NOTCOMPATIBLE")){
				System.out.println("HEY");
				return "index.xhtml";
			}
		} catch (IOException e) {
			System.err.println("Error while reading the csv");
			return "index.xhtml";
		}
		return "displayResult.xhtml";
	}

	//Getter & Setter
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public List<String> getListModelsName() {
		return listModelsName;
	}

	public void setListModelsName(List<String> listModelsName) {
		this.listModelsName = listModelsName;
	}
	
	public Part getCsvFilePart() {
		return csvFilePart;
	}

	public void setCsvFilePart(Part csvFilePart) {
		this.csvFilePart = csvFilePart;
	}

	public String getCsvFile() {
		return csvFile;
	}

	public void setCsvFile(String csvFile) {
		this.csvFile = csvFile;
	}

	public String getCsvFileOneLine() {
		return csvFileOneLine;
	}

	public void setCsvFileOneLine(String csvFileOneLine) {
		this.csvFileOneLine = csvFileOneLine;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getCsvRowSeparator() {
		return csvRowSeparator;
	}

	public void setCsvRowSeparator(String csvRowSeparator) {
		this.csvRowSeparator = csvRowSeparator;
	}

	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}

	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}
}