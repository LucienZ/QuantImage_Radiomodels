package ch.hevs.managedbeans;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import ch.hevs.businessobject.User;
import ch.hevs.quantimageservice.UserService;

/**
 * This class is the managedBean for the user operations
 * @author Lucien
 *
 */

public class UserManagedBean
{
    private UserService userService;
    private User user;
    private String firstName;
    private String lastName;
    private String eMail;
    private String password;

    /**
     * This method will initialize the managedBean
     */
    @PostConstruct
    public void initialize() {
    	// use JNDI to inject reference to Quantimage EJB
    	InitialContext ctx;
		try {
			ctx = new InitialContext();
	    	userService = (UserService) ctx.lookup("java:global/QuantImage_Radiomodels/UserServiceBean");
		} catch (NamingException e) {
			System.out.println("Can't initialize Context !");
			System.out.println(e.getMessage());
		}
    }
    
    /**
     * This method will connect the user
     * @return the index page
     */
    public String connect(){
    	password = encryptPassword(password);
    	user = userService.getUser(eMail, password);
    	
    	if(user == null){
    		if(userService.getUser(eMail)==null){
    			System.out.println("This account does not exists !");
    		}else{
        		System.out.println("The password is wrong !");	
    		}
    	}else{
    		System.out.println("UserConnected");
    	}
    	return "index.xhtml";
    }
    
    /**
     * This method will register a new User
     * @return the index page
     */
    public String register(){
    	password = encryptPassword(password);
		user = userService.registerUser(firstName, lastName, eMail, password);
		if(user!=null){
			System.out.println("User registered and connected !");
		}else{
			System.out.println("User already exists, please connect !");
		}
    	return "index.xhtml";
    }
    
    /**
     * This method will disconnect the user
     * @return the index page
     */
    public String disconnect(){
        user = null;
        firstName = "";
        lastName = "";
        eMail = "";
        password = "";
        
        return "index.xhtml";
    }
    
    /**
     * this method will encrypt the user password
     * @param unsecureText
     * @return the encrypted password
     */
    public String encryptPassword(String unsecureText){
    	String secureText = "";
    	MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
	    	byte[] hash = messageDigest.digest(unsecureText.getBytes(StandardCharsets.UTF_8));
	    	secureText = new String(hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}    	
    	return secureText;
    }
    
    //Getter and Setter
    
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}