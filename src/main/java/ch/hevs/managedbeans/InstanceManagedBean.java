package ch.hevs.managedbeans;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Part;

import ch.hevs.algorithmMethods.ClassifierMethods;
import ch.hevs.businessobject.QuantimageInstance;
import ch.hevs.quantimageservice.InstanceServiceStateful;
import ch.hevs.quantimageservice.InstanceServiceStateless;

/**
 * This bean is used to upload a CSV
 * @author Lucien
 *
 */
public class InstanceManagedBean
{
	private InstanceServiceStateful instanceFul;
	private InstanceServiceStateless instanceLess;
	private UserManagedBean userManagedBean;

	private boolean normalize;
	private String instanceName;
	private String csvFile;
	private String csvFileOneLine;
	private String csvFilePath;
	private String csvRowSeparator;
	private String choosenAlgorithm;
	private String evalResult;
	private int numberOfFolds = 10;
	private String className;
	private String modelName;
	private String evaluationType = "Cross Validation";
	private String uploadOrCreate;
	private String selectedHeaders;
	private String[] headers;
	private List<String> listInstanceName;
	private HashMap<String, String> choosenAlgorithmParametersHashMap;

	public final String SUPPORTVECTORMACHINEMODEL = "SupportVectorMachineModel";
	public final String RANDOMFOREST = "RandomForest";
	public final String MULTILAYERPERCEPTRON = "MultilayerPerceptron";
	public final String LOGISTIC = "Logistic";

	private Part csvFilePart;
	private boolean isClassifier = true;

	@PostConstruct
	public void initialize() {
		// use JNDI to inject reference to Quantimage EJB
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			instanceFul = (InstanceServiceStateful) ctx.lookup("java:global/QuantImage_Radiomodels/InstanceServiceBeanStateful");
			instanceLess = (InstanceServiceStateless) ctx.lookup("java:global/QuantImage_Radiomodels/InstanceServiceBeanStateless");
		} catch (NamingException e) {
			System.out.println("Can't initialize Context !");
			System.out.println(e.getMessage());		
		}		
	}

	/**
	 * This method will allow the update of a CSV, save it, create an instance and then the displayCSV it
	 * @return the displayCSV page
	 */
	@SuppressWarnings("resource")
	public String uploadCSV(){
		try {
			uploadOrCreate = "Upload File";
			csvFile = new Scanner(csvFilePart.getInputStream())
					.useDelimiter("\\A").next();

			csvFilePath = instanceFul.saveCSV(instanceName, csvFile);
			csvFileOneLine = csvFile.replaceAll("(\\r|\\n|\\r\\n)", "#");
			csvFileOneLine = csvFileOneLine.replaceAll("##", "#");
			selectHeaders();
			instanceFul.createQuantimageInstance(instanceName, csvFilePath, csvRowSeparator, userManagedBean.getUser(), normalize, headers);
			instanceFul.createInstance(csvFilePath, csvRowSeparator);
			return "displayCSV.xhtml";
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.err.println("Can't add the CSV");
		}  
		return "index.xhtml";
	}

	/**
	 * this method will select the headers
	 */
	public void selectHeaders(){
		BufferedReader br;
		try {
			br = new BufferedReader(new StringReader(csvFile));
			String newLine = br.readLine();
			headers = newLine.split(csvRowSeparator);

			br.close();

		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.err.println("Could not read the csv");
		}
	}

	/**
	 * This method will display the list of instances of an user
	 * @return the userPreviousInstance page
	 */
	public String displayExistingInstances(){
		uploadOrCreate = "Use previous instance";
		listInstanceName = instanceLess.getInstancesByUser(userManagedBean.getUser().getId());

		return "usePreviousInstance.xhtml";
	}

	/**
	 * This method will perform the operations to use an existing model
	 * @return the displayCSV page
	 */
	public String useExistingModel(){
		QuantimageInstance selectedInstance = instanceLess.getInstancesByName(instanceName);
		instanceFul.initiateQuantimageInstance(instanceName);
		normalize = selectedInstance.isNormalize();
		csvRowSeparator = selectedInstance.getCsvRowSeparator();
		csvFilePath = selectedInstance.getCsvFilePath();
		csvFile = instanceLess.getCSVFile(csvFilePath);
		csvFileOneLine = csvFile.replaceAll("(\\r|\\n|\\r\\n)", "#");
		csvFileOneLine = csvFileOneLine.replaceAll("##", "#");
		selectHeaders();    

		instanceFul.createInstance(csvFilePath, csvRowSeparator);
		return "displayCSV.xhtml";
	}

	/**
	 * This method will set the class and normalize the instance if the checkbox is checked
	 * 
	 * @return the applyAlgorithm page
	 */
	public String submitPreprocessing(){
		instanceFul.submitPreprocessing(normalize, className, selectedHeaders);
		return "applyAlgorithm.xhtml";
	}

	/**
	 * This method will choose the correct algorithm and set up the list of parameters
	 * @return the applyParameters page
	 */
	public String chooseAlgorithm(){
		HashMap<String, String> choosenAlgorithmHashMap = ClassifierMethods.choosenAlgoParameters.get(choosenAlgorithm);
		HashMap<String, String> choosenAlgorithmDefaultParameterHashMap = ClassifierMethods.choosenAlgoDefaultParameters.get(choosenAlgorithmHashMap);
		choosenAlgorithmParametersHashMap = new HashMap<String, String>();
		Iterator<Entry<String, String>> it = choosenAlgorithmHashMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
			String key = pair.getKey();
			String description = pair.getValue();
			String parameterValue = choosenAlgorithmDefaultParameterHashMap.get(key);

			choosenAlgorithmParametersHashMap.put(description, parameterValue);
		}
		return "applyParameters.xhtml";
	}

	/**
	 * This method will apply the selected algorithm to the instance
	 * @return the evaluation display
	 */
	public String applyAlgorithm(){
		evalResult = instanceFul.AlgorithmMethod(choosenAlgorithmParametersHashMap, choosenAlgorithm, numberOfFolds);
		return "displayEvaluation.xhtml"; 
	}

	/**
	 * This method will save the instance in rootJavaE/tools/wildfly-?/bin/data/Models/instanceName.csv
	 * @return The index.xhtml page
	 * @throws FileNotFoundException
	 */
	public String serializeModel() throws FileNotFoundException{
		instanceFul.createQuantimageModel(modelName, choosenAlgorithm, evalResult, numberOfFolds, evaluationType, isClassifier, selectedHeaders);
		return "index.xhtml";
	}

	//List of algorithms
	private static Map<String, String> listAlgorithm;
	static{
		listAlgorithm = new LinkedHashMap<String, String>();
//		listAlgorithm.put("SupportVectorMachineModel", "Support Vector Machine Model");
//		listAlgorithm.put("RandomForest", "Random Forest");
		listAlgorithm.put("MultilayerPerceptron", "Multilayer Perceptron");
		listAlgorithm.put("Logistic", "Logistic");
	}

	//Getter and Setter

	public Map<String, String> getListAlgorithm() {
		return listAlgorithm;
	}

	public void setListAlgorithm(Map<String, String> listAlgorithm) {
		InstanceManagedBean.listAlgorithm = listAlgorithm;
	}

	//Getter And Setter
	public String getChoosenAlgorithm() {
		return choosenAlgorithm;
	}

	public void setChoosenAlgorithm(String choosenAlgorithm) {
		this.choosenAlgorithm = choosenAlgorithm;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public UserManagedBean getUserBean() {
		return userManagedBean;
	}

	public void setUserBean(UserManagedBean userBean) {
		this.userManagedBean = userBean;
	}

	public boolean isNormalize() {
		return normalize;
	}

	public void setNormalize(boolean normalize) {
		this.normalize = normalize;
	}

	public Part getCsvFilePart() {
		return csvFilePart;
	}

	public void setCsvFilePart(Part csvFilePart) {
		this.csvFilePart = csvFilePart;
	}

	public String getCsvFile() {
		return csvFile;
	}

	public void setCsvFile(String csvFile) {
		this.csvFile = csvFile;
	}

	public String[] getHeaders() {
		return headers;
	}

	public void setHeaders(String[] headers) {
		this.headers = headers;
	}

	public String getCsvFilePath() {
		return csvFilePath;
	}

	public void setCsvFilePath(String csvFilePath) {
		this.csvFilePath = csvFilePath;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getCsvFileOneLine() {
		return csvFileOneLine;
	}

	public void setCsvFileOneLine(String csvFileOneLine) {
		this.csvFileOneLine = csvFileOneLine;
	}

	public String getCsvRowSeparator() {
		return csvRowSeparator;
	}

	public void setCsvRowSeparator(String csvRowSeparator) {
		this.csvRowSeparator = csvRowSeparator;
	}

	public String getEvalResult() {
		return evalResult;
	}

	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult;
	}

	public int getNumberOfFolds() {
		return numberOfFolds;
	}

	public void setNumberOfFolds(int numberOfFolds) {
		this.numberOfFolds = numberOfFolds;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getEvaluationType() {
		return evaluationType;
	}

	public void setEvaluationType(String evaluationType) {
		this.evaluationType = evaluationType;
	}

	public boolean isClassifier() {
		return isClassifier;
	}

	public void setClassifier(boolean isClassifier) {
		this.isClassifier = isClassifier;
	}

	public HashMap<String, String> getChoosenAlgorithmParametersHashMap() {
		return choosenAlgorithmParametersHashMap;
	}

	public void setChoosenAlgorithmParametersHashMap(HashMap<String, String> choosenAlgorithmParametersHashMap) {
		this.choosenAlgorithmParametersHashMap = choosenAlgorithmParametersHashMap;
	}
	public UserManagedBean getUserManagedBean() {
		return userManagedBean;
	}

	public void setUserManagedBean(UserManagedBean userManagedBean) {
		this.userManagedBean = userManagedBean;
	}

	public List<String> getListInstanceName() {
		return listInstanceName;
	}

	public void setListInstanceName(List<String> listInstanceName) {
		this.listInstanceName = listInstanceName;
	}
	public String getSelectedHeaders() {
		return selectedHeaders;
	}

	public void setSelectedHeaders(String selectedHeaders) {
		this.selectedHeaders = selectedHeaders;
	}

	public String getUploadOrCreate() {
		return uploadOrCreate;
	}

	public void setUploadOrCreate(String uploadOrCreate) {
		this.uploadOrCreate = uploadOrCreate;
	}
}