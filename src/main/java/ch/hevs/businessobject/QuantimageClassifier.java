package ch.hevs.businessobject;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class QuantimageClassifier extends QuantimageAlgorithm{

	@Column(name="classifierName")
	private String classifierName;
	
	//Classifier Name
	public String getClassifierName() {
		return classifierName;
	}

	public void setClassifierName(String classifierName) {
		this.classifierName = classifierName;
	}

	//Constructor
	public QuantimageClassifier() {

	}

	public QuantimageClassifier(String options, String classifierName) {
		super(options);
		this.classifierName = classifierName;
	}

}
