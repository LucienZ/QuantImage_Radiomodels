package ch.hevs.businessobject;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class QuantimageInstance {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	@Column(name="name")
	private String name;
	@Column(name="csvFilePath")
	private String csvFilePath;
	@Column(name="csvRowSeparator")
	private String csvRowSeparator;
	@Column(name="normalize")
	private boolean normalize;
	@Column(name="features")
	private String features;
	
	@OneToMany(mappedBy = "instance", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<QuantimageModel> models;
	
	@ManyToOne
	@JoinColumn(name = "FK_USER")
	private User owner;
	
	//Id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	//Name
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//CsvFile
	public String getCsvFilePath() {
		return csvFilePath;
	}

	public void setCsvFilePath(String csvFile) {
		this.csvFilePath = csvFile;
	}
	
	//Owner
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}	
	
	//csvRowSeparator
	public String getCsvRowSeparator() {
		return csvRowSeparator;
	}
	public void setCsvRowSeparator(String csvRowSeparator) {
		this.csvRowSeparator = csvRowSeparator;
	}
	
	//Features
	public String getFeatures() {
		return features;
	}
	public void setFeatures(String features) {
		this.features = features;
	}
	
	//Normalize
	public boolean isNormalize() {
		return normalize;
	}
	public void setNormalize(boolean normalize) {
		this.normalize = normalize;
	}

	//Models
	public List<QuantimageModel> getModels() {
		return models;
	}

	public void setModels(List<QuantimageModel> models) {
		this.models = models;
	}
	
	public void addModel(QuantimageModel model){
		model.setInstance(this);
		this.models.add(model);
	}
	
	//Constructor
	public QuantimageInstance() {

	}
	
	public QuantimageInstance(String name, String csvFilePath, String csvRowSeparator, User owner, boolean normalize, String features) {
		this.name = name;
		this.csvFilePath = csvFilePath;
		this.csvRowSeparator = csvRowSeparator;
		this.owner = owner;
		this.normalize = normalize;
		this.features = features;
		this.models =  new ArrayList<QuantimageModel>();
	}
	
	//ToString
	@Override
	public String toString() {
		return "QuantimageInstance [id=" + id + ", name=" + name + ", csvFilePath=" + csvFilePath + ", csvRowSeparator="
				+ csvRowSeparator + ", normalize=" + normalize + ", features=" + features + ", models=" + models
				+ ", owner=" + owner + "]";
	}
	
	
}
