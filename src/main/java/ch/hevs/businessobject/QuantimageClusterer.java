package ch.hevs.businessobject;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class QuantimageClusterer extends QuantimageAlgorithm{	
	
	@Column(name="clustererName")
	private String clustererName;	
	
	//Clusterer Name
	public String getClustererName() {
		return clustererName;
	}

	public void setClustererName(String clustererName) {
		this.clustererName = clustererName;
	}
	
	// constructors
	public QuantimageClusterer() {

	}

	public QuantimageClusterer(String options, String clustererName) {
		super(options);
		this.clustererName = clustererName;
	}
}