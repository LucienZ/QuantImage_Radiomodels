package ch.hevs.businessobject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class QuantimageAlgorithm {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private String options;
	
	// relations
	@OneToOne
	@JoinColumn(name = "FK_MODEL")
	private QuantimageModel model;

	// id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	//option
	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	//model
	public QuantimageModel getModel() {
		return model;
	}

	public void setModel(QuantimageModel model) {
		this.model = model;
	}

	// constructors
	public QuantimageAlgorithm() {

	}

	public QuantimageAlgorithm(String options) {
		super();
		this.options = options;
	}
	//To String
	@Override
	public String toString() {
		return "id - " + id + ", name - " + model + ", options - " + options;
	}
	
	
}
