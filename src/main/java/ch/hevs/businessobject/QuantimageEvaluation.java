package ch.hevs.businessobject;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class QuantimageEvaluation {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	@Column(name="evaluationType")
	private String evaluationType;	
	@Column(name="numberOfFolds")
	private int numberOfFolds;
	@Column(name="result")
	private String result;	

	
	// relations
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_MODEL", nullable = true)
	private QuantimageModel model;

	//Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEvaluationType() {
		return evaluationType;
	}

	public void setEvaluationType(String evaluationType) {
		this.evaluationType = evaluationType;
	}
	
	//number Of Folds
	public int getNumberOfFolds() {
		return numberOfFolds;
	}

	public void setNumberOfFolds(int numberOfFolds) {
		this.numberOfFolds = numberOfFolds;
	}

	//Value
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	//Model
	public QuantimageModel getModel() {
		return model;
	}

	public void setModel(QuantimageModel model) {
		this.model = model;
	}

	//Constructor
	public QuantimageEvaluation() {
		super();
	}

	public QuantimageEvaluation(int numberOfFolds, String result, String evaluationType) {
		this.evaluationType = evaluationType;
		this.numberOfFolds = numberOfFolds;
		this.result = result;
	}
	
	//To String
	@Override
	public String toString() {
		return "evaluationType - " + evaluationType + ", numberOfFolds - " + numberOfFolds + ", result - " + result + ", model - " + model;
	}	
}
