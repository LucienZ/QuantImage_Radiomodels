package ch.hevs.businessobject;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class QuantimageModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	@Column(name="name")
	private String name;
	@Column(name="classAttributeName")
	private String classAtributeName;
	@Column(name="modelPath")
	private String modelPath;
	@Column(name="IgnoredFeatures")
	private String ignoredFeatures;

	// relations	
	@OneToOne(mappedBy = "model", cascade = CascadeType.ALL)
	private QuantimageAlgorithm algorithm;
	
	@OneToOne(mappedBy = "model", cascade = CascadeType.ALL)
	private QuantimageEvaluation evaluation;
	
	@ManyToOne
	@JoinColumn(name = "FK_INSTANCE")
	private QuantimageInstance instance;
	
	// id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	// name
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//ModelPath
	public String getModelPath() {
		return modelPath;
	}
	
	public void setModelPath(String modelPath) {
		this.modelPath = modelPath;
	}
	
	//Class Attribute name
	public String getClassAtributeName() {
		return classAtributeName;
	}
	public void setClassAtributeName(String classAtributeName) {
		this.classAtributeName = classAtributeName;
	}
	
	//Ignored Features
	public String getIgnoredFeatures() {
		return ignoredFeatures;
	}
	public void setIgnoredFeatures(String ignoredFeatures) {
		this.ignoredFeatures = ignoredFeatures;
	}
	
	//Algorithm
	public QuantimageAlgorithm getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(QuantimageAlgorithm algorithm) {
		algorithm.setModel(this);
		this.algorithm = algorithm;
	}
	
	//Evaluations
	public QuantimageEvaluation getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(QuantimageEvaluation evaluation) {
		this.evaluation = evaluation;
		this.evaluation.setModel(this);

	}
	//Instance
	public QuantimageInstance getInstance() {
		return instance;
	}

	public void setInstance(QuantimageInstance instance) {
		this.instance = instance;
	}
	// constructors
	public QuantimageModel() {
	
	}
	
	public QuantimageModel(String name, String modelPath, String classAttributeName, String ignoredFeatures) {
		this.name = name;
		this.modelPath = modelPath;
		this.classAtributeName = classAttributeName;
		this.ignoredFeatures = ignoredFeatures;
	}
}
