var removedHeaders = [];
function displayCSV(data, rowSeparator){
	// start the table
	var html = '<table id="htmlTableCSV">';

	// split into lines
	var rows = data.split("#");
	
	//Set the table headers
	// start a table row
	html += "<tr>";
	// split line into columns
	var columns = rows[0].split(rowSeparator);
	for (var j = 0; j < columns.length; j++){
		column = columns[j];
		var addHeader = "<th style='margin: 70px; border: 1px solid black' onclick='headerClick(\"" + column + "\")'>" + column + "</th>"
		html += addHeader;
	}
	// close row
	html += "</tr>";
	//Set the table data		
	// parse lines
	for (var i = 1; i < rows.length; i++){
		var row = rows[i];
		// start a table row
		html += "<tr>";
		// split line into columns
		var columns = row.split(rowSeparator);
		for (var j = 0; j < columns.length; j++){
			column = columns[j];
			html += "<td>" + column + "</td>";
		}
		// close row
		html += "</tr>";
	}
	// close table
	html += "</table>";

	// insert into div
	$('#CSVTable').append(html);
	$("th").click(function(){
		console.log("click")
	    if(!$(this).hasClass("active")) {
	        $(this).addClass("active");
	    } else {
	        $(this).removeClass("active");
	    }
	});
}

function headerClick(attribute){
	if(removedHeaders.indexOf(attribute)==-1){
		removedHeaders.push(attribute)
	}else{
		removedHeaders.splice(attribute, 1);
	}
}

function getRemovedHeaders(){
	removedHeadersString = "";
	if(removedHeaders.length > 0){
		for (i = 0; i < removedHeaders.length-1; i++) {
			removedHeadersString += removedHeaders[i]+",";
		}
		removedHeadersString += removedHeaders[removedHeaders.length-1]
	}
	document.getElementById("displayCSVForm:selectedHeaders").value = removedHeadersString;
}